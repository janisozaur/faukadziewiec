/*
Copyright(c) 2017 Christopher Joseph Dean Schaefer

This software is provided 'as-is', without any express or implied
warranty.In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions :

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software.If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

/*
https://msdn.microsoft.com/en-us/library/windows/desktop/bb172256(v=vs.85).aspx
*/
vec4 GetGlobalIllumination()
{
	vec4 ambient = vec4(0);
	vec4 diffuse = vec4(0);
	vec4 specular = vec4(0);
	vec4 emissive = vec4(0);	
	vec4 ambientTemp = vec4(0);
	vec4 diffuseTemp = vec4(0);
	vec4 specularTemp = vec4(0);
	vec3 cameraPosition = vec3(0);

	float lightDistance = 0;
	vec4 vectorPosition = vec4(0);
	vec4 lightPosition = vec4(0);
	vec4 lightDirection = vec4(0);
	float attenuation = 0;
	vec4 ldir = vec4(0);
	float rho = 0;
	float spot = 0;

	vectorPosition = ubo.modelTransformation * vec4(position.xyz,1.0);
	vectorPosition *= vec4(1.0,-1.0,1.0,1.0);

	//https://msdn.microsoft.com/en-us/library/windows/desktop/bb172279(v=vs.85).aspx
	for( int i=0; i<8; ++i )
	{		
		if(shaderState.mLights[i].IsEnabled)
		{		
			lightPosition = ubo.modelTransformation * shaderState.mLights[i].Position;
			lightPosition *= vec4(1.0,-1.0,1.0,1.0);

			lightDirection = shaderState.mLights[i].Direction;
			lightDirection *= vec4(1.0,-1.0,1.0,1.0);

			lightDistance = abs(distance(vectorPosition.xyz,lightPosition.xyz));

			if(shaderState.mLights[i].Type == D3DLIGHT_DIRECTIONAL)
			{
				ldir = normalize(lightDirection * vec4(-1.0,-1.0,-1.0,-1.0));
			}
			else
			{
				ldir = normalize(lightPosition - vectorPosition);
			}

			if(shaderState.mLights[i].Type == D3DLIGHT_DIRECTIONAL)
			{
				attenuation = 1;
			}
			else if(shaderState.mLights[i].Range < lightDistance)
			{
				attenuation = 0;
			}
			else
			{
				attenuation = 1/( shaderState.mLights[i].Attenuation0 + shaderState.mLights[i].Attenuation1 * lightDistance + shaderState.mLights[i].Attenuation2 * pow(lightDistance,2));	
			}

			rho = clamp(dot(normalize(lightDirection.xyz * vec3(-1.0,-1.0,-1.0)),normalize(lightPosition.xyz - vectorPosition.xyz)),0,1);

			if(shaderState.mLights[i].Type != D3DLIGHT_SPOT || rho > cos(shaderState.mLights[i].Theta/2))
			{
				spot = 1;
			}
			else if(rho <= cos(shaderState.mLights[i].Phi/2))
			{
				spot = 0;
			}
			else
			{
				float u = rho - cos(shaderState.mLights[i].Phi / 2);
				float v = cos(shaderState.mLights[i].Theta / 2) - cos(shaderState.mLights[i].Phi / 2);
				spot = pow(clamp(u / v,0,1),shaderState.mLights[i].Falloff);
			}

			ambientTemp += (attenuation * spot * shaderState.mLights[i].Ambient);
			diffuseTemp += (diffuseColor * shaderState.mLights[i].Diffuse * clamp(dot(normal,ldir),0,1) * attenuation * spot);

			if(shaderState.specularEnable==1)
			{
				vec3 halfway;
				if(shaderState.localViewer==1)
				{
					halfway = normalize(normalize(cameraPosition - pos.xyz) + ldir.xyz);			
				}
				else
				{
					halfway = normalize(vec3(0,0,1) + ldir.xyz);
				}
				
				//The result is defined if x < 0 or x=0 and y<=0
				// The clamp will take care of the former and the if will take care of the latter. if the result is zero then this whole line is zero therefore just skip it.
				if(shaderState.mMaterial.Power > 0)
				{
					specularTemp += (shaderState.mLights[i].Specular * pow(clamp(dot(normal.xyz,halfway),0,1),shaderState.mMaterial.Power) * attenuation * spot);
				}
			}
		}
	}

	ambient = shaderState.mMaterial.Ambient * (Convert(shaderState.ambient) + ambientTemp);
	diffuse = diffuseTemp;
	emissive = emissiveColor;

	if(shaderState.specularEnable==1)
	{
		specular = specularColor * specularTemp;
	}

	vec4 lightColor = (ambient + diffuse + specular + emissive);
	lightColor.a = 1.0;
	return lightColor;
}