/*
Copyright(c) 2016 Christopher Joseph Dean Schaefer

This software is provided 'as-is', without any express or implied
warranty.In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions :

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software.If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#include "Constants"
#include "Structures"
#include "Functions"

layout(std140,binding = 0) uniform ShaderStateBlock
{
	ShaderState shaderState;
};

layout(binding = 3) uniform sampler2D textures[2];

layout(push_constant) uniform UniformBufferObject 
{
    mat4 totalTransformation;
	mat4 modelTransformation;
} ubo;

layout (location = 0) in vec4 diffuseColor;
layout (location = 1) in vec4 ambientColor;
layout (location = 2) in vec4 specularColor;
layout (location = 3) in vec4 emissiveColor;
layout (location = 4) in vec4 normal;
layout (location = 5) in vec2 texcoord1;
layout (location = 6) in vec2 texcoord2;
layout (location = 7) in vec4 pos;
layout (location = 8) in vec4 globalIllumination;
layout (location = 0) out vec4 uFragColor;

vec2 getTextureCoord(int index)
{
	switch(index)
	{
		case 0:
			return texcoord1;
		break;
		case 1:
			return texcoord2;
		break;
		default:
			return vec2(0,0);
		break;
	}
}

#include "FixedFunctions"

void main() 
{
	vec4 temp = vec4(1.0,1.0,1.0,1.0);
	vec4 result = vec4(1.0,1.0,1.0,1.0); //On stage 0 CURRENT is the same as DIFFUSE

	if(shaderState.textureCount>0)
	{
		processStage(textures[0],shaderState.mTextureStages[0].texureCoordinateIndex, shaderState.mTextureStages[0].Constant, shaderState.mTextureStages[0].Result,
		result, temp, result, temp,
		shaderState.mTextureStages[0].colorOperation, shaderState.mTextureStages[0].colorArgument1, shaderState.mTextureStages[0].colorArgument2, shaderState.mTextureStages[0].colorArgument0,
		shaderState.mTextureStages[0].alphaOperation, shaderState.mTextureStages[0].alphaArgument1, shaderState.mTextureStages[0].alphaArgument2, shaderState.mTextureStages[0].alphaArgument0);
	}

	if(shaderState.textureCount>1)
	{
		processStage(textures[1],shaderState.mTextureStages[1].texureCoordinateIndex, shaderState.mTextureStages[1].Constant, shaderState.mTextureStages[1].Result,
		result, temp, result, temp,
		shaderState.mTextureStages[1].colorOperation, shaderState.mTextureStages[1].colorArgument1, shaderState.mTextureStages[1].colorArgument2, shaderState.mTextureStages[1].colorArgument0,
		shaderState.mTextureStages[1].alphaOperation, shaderState.mTextureStages[1].alphaArgument1, shaderState.mTextureStages[1].alphaArgument2, shaderState.mTextureStages[1].alphaArgument0);
	}
		
	uFragColor = result;
	
	if(shaderState.lighting==1)
	{	
		if(shaderState.shadeMode == D3DSHADE_GOURAUD)
		{
			uFragColor.rgb *= globalIllumination.rgb;
		}
	}
}
